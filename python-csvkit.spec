%global _empty_manifest_terminate_build 0
Name:		python-csvkit
Version:	2.0.1
Release:	1
Summary:	A suite of command-line tools for working with CSV, the king of tabular file formats.
License:	MIT
URL:		https://github.com/wireservice/csvkit
Source0:	https://files.pythonhosted.org/packages/b6/29/51d7c3221669a4a63410f9be61178436109217d77a31b539f41ef6c1448e/csvkit-2.0.1.tar.gz
BuildArch:	noarch

Requires:	python3-agate
Requires:	python3-agate-excel
Requires:	python3-agate-dbf
Requires:	python3-agate-sql
Requires:	python3-six
Requires:	python3-setuptools
Requires:	python3-argparse
Requires:	python3-ordereddict
Requires:	python3-simplejson
Requires:	python3-sphinx
Requires:	python3-sphinx-rtd-theme
Requires:	python3-coverage
Requires:	python3-nose
Requires:   python3-SQLAlchemy

%description
A suite of utilities for converting to and working with CSV, the king of tabular file formats.

%package -n python3-csvkit
Summary:	A suite of command-line tools for working with CSV, the king of tabular file formats.
Provides:	python-csvkit
BuildRequires:	python3-devel
BuildRequires:	zlib >= 1.2.11
BuildRequires:	python3-setuptools
%description -n python3-csvkit
A suite of utilities for converting to and working with CSV, the king of tabular file formats.

%package help
Summary:	Development documents and examples for csvkit
Provides:	python3-csvkit-doc
%description help
A suite of utilities for converting to and working with CSV, the king of tabular file formats.

%prep
%autosetup -n csvkit-2.0.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-csvkit -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Aug 9 2024 lilu <lilu@kylinos.cn> - 2.0.1-1
- Update package to version 2.0.1
- feat: csvsql adds --min-col-len and --col-len-multiplier options
- fix: csvstat no longer errors when a column is a time delta and --json is set

* Mon Jun 24 2024 xieyanlong <xieyanlong@kylinos.cn> - 2.0.0-1
- fix: The --join-short-rows option no longer reports length mismatch errors that were fixed.
- fix: Use the --locale option to set the locale of any formatted numbers.
- The --quoting option accepts 4 and 5 on Python 3.12.

* Sun Apr 24 2022 liqiuyu <liqiuyu@kylinos.cn> - 1.0.6-2
- Add zlib to buildrequires 

* Thu Dec 09 2021 Python_Bot <Python_Bot@openeuler.org> - 1.0.6-1
- Package Init
